package com.jean.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="informacion_producto")
public class EntidadInformacionProductos {

	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="Descripcion")
	private String descripcion ;
	
	@OneToOne
	@JoinColumn(name="Producto_Id")
	private EntidadProducto miIdProducto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public EntidadProducto getMiIdProducto() {
		return miIdProducto;
	}

	public void setMiIdProducto(EntidadProducto miIdProducto) {
		this.miIdProducto = miIdProducto;
	}

	@Override
	public String toString() {
		return "EntidadInformacionProductos [id=" + id + ", descripcion=" + descripcion + ", miIdProducto="
				+ miIdProducto + "]";
	}
		
	
}
