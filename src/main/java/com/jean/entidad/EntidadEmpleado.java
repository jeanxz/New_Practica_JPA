package com.jean.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Empleado")
public class EntidadEmpleado {

	@Id
	@Column(name="Id")
	private int id;
	
	@Column(name="Nombre")
	private String nombre;
	
	@OneToOne (mappedBy="miIdEmpleado")
	private EntidadOficina miOficina;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public EntidadOficina getMiOficina() {
		return miOficina;
	}
	public void setMiOficina(EntidadOficina miOficina) {
		this.miOficina = miOficina;
	}
	@Override
	public String toString() {
		return "EntidadEmpleado [id=" + id + ", nombre=" + nombre + ", miOficina=" + miOficina + "]";
	}
	
	
	
		
}
