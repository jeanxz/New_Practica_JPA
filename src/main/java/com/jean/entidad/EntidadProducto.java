package com.jean.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Producto")
@NamedQueries({
	@NamedQuery(name="EntidadProducto.BuscarPorID",query="select a.miProductoInform from  EntidadProducto a where a.nombre=:miProducto")
})
public class EntidadProducto {

	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="Nombre")
	private String nombre;
	
	@Column(name="Precio")
	private int precio;
	
	@OneToOne(mappedBy="miIdProducto")
	private EntidadInformacionProductos miProductoInform;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public EntidadInformacionProductos getMiProductoInform() {
		return miProductoInform;
	}

	public void setMiProductoInform(EntidadInformacionProductos miProductoInform) {
		this.miProductoInform = miProductoInform;
	}

	@Override
	public String toString() {
		return "EntidadProducto [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", miProductoInform="
				+ miProductoInform + "]";
	}
	
	
	
	
	
}
