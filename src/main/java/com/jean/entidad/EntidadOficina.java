package com.jean.entidad;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Oficina")
@NamedQueries({
	@NamedQuery(name="EntidadOficina.mostrarOficina",query="select a from EntidadOficina a")
})
public class EntidadOficina {

	@Id
	@Column(name="Id")
	private int id ;
	
	@Column (name="No_Piso")
	private int noPiso;
	
	@OneToOne
	@JoinColumn(name="Id_Empleado")
	private EntidadEmpleado miIdEmpleado;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNoPiso() {
		return noPiso;
	}

	public void setNoPiso(int noPiso) {
		this.noPiso = noPiso;
	}

	public EntidadEmpleado getMiIdEmpleado() {
		return miIdEmpleado;
	}

	public void setMiIdEmpleado(EntidadEmpleado miIdEmpleado) {
		this.miIdEmpleado = miIdEmpleado;
	}

	@Override
	public String toString() {
		return "EntidadOficina [id=" + id + ", noPiso=" + noPiso + ", miIdEmpleado=" + miIdEmpleado + "]";
	}

	
	
}
