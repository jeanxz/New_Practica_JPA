package com.jean.utiliarios;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.jean.entidadfamilia.EntidadEsposa;
import com.jean.entidadfamilia.EntidadEsposo;

public class UtilidadFamilia {
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("Conect");
	
	EntityManager emEspo= emf.createEntityManager();
	
	public String miPadreName(String miHijo){
		
		EntidadEsposo miEsposo= emEspo.createNamedQuery("EntidadHijo.ObtenerPadre",EntidadEsposo.class).setParameter("nombreHijo",miHijo).getSingleResult();
	
		return miEsposo.getNombre();
	}

}
