package com.jean.utiliarios;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.jean.entidad.EntidadInformacionProductos;
import com.jean.entidad.EntidadProducto;

public class UtilidadProductos {
	
EntityManagerFactory emf = Persistence.createEntityManagerFactory("Conect");
	
	EntityManager emProducto= emf.createEntityManager();
	
	
	
	public void actualizarDescripcionProducto(String nombre, String decripcion){
		
		
		emProducto.getTransaction().begin();
		EntidadInformacionProductos miProducto = emProducto.createNamedQuery("EntidadProducto.BuscarPorID",EntidadInformacionProductos.class).setParameter("miProducto",nombre).getSingleResult();
		
		miProducto.setDescripcion(decripcion);
		
		
		emProducto.getTransaction().commit();
		System.out.println("--Producto actualizado correctamente--");
		System.out.println("----------------------------------------");
		System.out.println(miProducto.getDescripcion());
		
		
	}
}
