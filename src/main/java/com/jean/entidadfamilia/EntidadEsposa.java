package com.jean.entidadfamilia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@Table(name="Esposa")
@NamedQueries({
	@NamedQuery(name="EntidadEsposa.MostarDatos",query="Select a.miEsposo from EntidadEsposa a where a.miHijo.nombre=:nombreHijo")
})
public class EntidadEsposa {
	
	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int Id;
	
	@Column(name="Nombre")
	private String nombre;
	
	@OneToOne
	@JoinColumn(name="Id_Hijo")
	private EntidadHijo miHijo;
	
	
	@OneToOne(mappedBy="miEsposa")
	private EntidadEsposo miEsposo;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public EntidadHijo getMiHijo() {
		return miHijo;
	}

	public void setMiHijo(EntidadHijo miHijo) {
		this.miHijo = miHijo;
	}

	public EntidadEsposo getMiEsposo() {
		return miEsposo;
	}

	public void setMiEsposo(EntidadEsposo miEsposo) {
		this.miEsposo = miEsposo;
	}

	@Override
	public String toString() {
		return "EntidadEsposa [Id=" + Id + ", nombre=" + nombre + ", miHijo=" + miHijo + ", miEsposo=" + miEsposo + "]";
	}

	
	
	
}
