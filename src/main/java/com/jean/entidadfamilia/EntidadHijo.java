package com.jean.entidadfamilia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Hijo")
@NamedQueries({
	@NamedQuery(name="EntidadHijo.ObtenerPadre",query="Select a.miMadre.miEsposo from EntidadHijo a where a.nombre=:nombreHijo")
})
public class EntidadHijo {
	
	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id ;
	
	@Column(name="Nombre")
	private String nombre;
	
	@OneToOne(mappedBy="miHijo")
	private EntidadEsposa miMadre;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public EntidadEsposa getMiMadre() {
		return miMadre;
	}

	public void setMiMadre(EntidadEsposa miMadre) {
		this.miMadre = miMadre;
	}

	@Override
	public String toString() {
		return "EntidadHijo [id=" + id + ", nombre=" + nombre + ", miMadre=" + miMadre + "]";
	}
	
	
}
