package com.jean.entidadfamilia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Esposo")
@NamedQueries({
	@NamedQuery(name="EntidadEsposa.MostarDatos",query="Select a.miEsposo from EntidadEsposa a where a.miHijo.nombre=:nombreHijo")
})
public class EntidadEsposo {

	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="Nombre")
	private String nombre ;
	
	@OneToOne
	@JoinColumn(name="Id_Esposa")
	private EntidadEsposa miEsposa ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public EntidadEsposa getMiEsposa() {
		return miEsposa;
	}

	public void setMiEsposa(EntidadEsposa miEsposa) {
		this.miEsposa = miEsposa;
	}

	@Override
	public String toString() {
		return "EntidadEsposo [id=" + id + ", nombre=" + nombre + ", miEsposa=" + miEsposa + "]";
	}
	
	
	
}
